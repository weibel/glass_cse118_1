package com.groupone.glassscan;

import android.os.Build;
import android.os.Bundle;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.view.Menu;
import android.view.WindowManager;
import android.content.Intent;
import android.view.View;
//import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.groupone.glassscan.Food;
import com.groupone.glassscan.R;
import com.parse.*;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class MainActivity extends Activity implements OnClickListener {

	private Button scanBtn;
	//private TextView firstNameTxt, lastNameTxt, ageTxt, genderTxt, IDTxt;
	private TextView nameTxt, idTxt, nutritionTxt, introTxt;	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// requestWindowFeature(Window.FEATURE_NO_TITLE);

		System.out.println("About to initialise Parse");
		Parse.initialize(this, "CNWcbJXoR5UDVjlsyiMvL3IrxJefNSbyhmbSpuWt",
				"mdEtEBxNcyxeKzxLjxh1ydaZBhBZ1sbdaztRT11c");
		System.out.println("Parse: tracking app opening");
		ParseAnalytics.trackAppOpened(getIntent());

		setScanBtn((Button) findViewById(R.id.scan_button));
		// setFormatTxt((TextView) findViewById(R.id.scan_format));
		// setContentTxt((TextView) findViewById(R.id.scan_content));

		
		 ActionBar actionBar = getActionBar();
		 actionBar.hide();
		 
		//Remove title bar
		 //this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		 //Remove notification bar
		 this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		 
		scanBtn.setOnClickListener(this);

	}
	

//	public static TimelineItem insertTimelineItem(Mirror service, String text,
//			String contentType, InputStream attachment, String notificationLevel) {
//		TimelineItem timelineItem = new TimelineItem();
//		timelineItem.setText(text);
//		if (notificationLevel != null && notificationLevel.length() > 0) {
//			timelineItem.setNotification(new NotificationConfig()
//					.setLevel(notificationLevel));
//		}
//		try {
//			if (contentType != null && contentType.length() > 0
//					&& attachment != null) {
//				// Insert both metadata and attachment.
//
//				InputStreamContent mediaContent = new InputStreamContent(
//						contentType, attachment);
//				return service.timeline().insert(timelineItem, mediaContent)
//						.execute();
//
//			} else {
//				// Insert metadata only.
//				return service.timeline().insert(timelineItem).execute();
//			}
//		} catch (IOException e) {
//			System.err.println("An error occurred: " + e);
//			return null;
//		}
//	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void showInfoScreen(View view){
		Intent intent = new Intent(this, InfoScreen.class);
	    String productName = "Nissin Top Ramen - Beef";
	    String calories = "Calories: 190";
	    String sodium =  "Sodium: 760mg";  
	    String carbs = "Carbs: 27g";
	    String fat = "Fat: 7g";
	    String protein = "Protein: 5g";
	    String sugar = "Sugar: 0g";
	    String vegetarian = "false";
	    String vegan = "false";
	    String gluten_free = "false";
	    String trans_fat = 	"Trans Fat: 0g";
	    
		intent.putExtra("ProductName",productName);		
		intent.putExtra("calories",calories);			
		intent.putExtra("sodium",sodium);	
		intent.putExtra("carbs",carbs);	
		intent.putExtra("fat",fat);	
		intent.putExtra("protein",protein);	
		intent.putExtra("sugar",sugar);	
		intent.putExtra("vegetarian",vegetarian);	
		intent.putExtra("vegan",vegan);	
		intent.putExtra("gluten_free",gluten_free);	
		intent.putExtra("trans_fat",trans_fat);	
		
		startActivity(intent);
	}

	public Button getScanBtn() {
		return scanBtn;
	}

	public void setScanBtn(Button scanBtn) {
		this.scanBtn = scanBtn;
	}


	@Override
	public void onClick(View v) {
		// respond to clicks for OnClickListeners
		if (v.getId() == R.id.scan_button) {
			// scan
			// IntentIntegrator scanIntegrator = new IntentIntegrator(this);
			// scanIntegrator.initiateScan();

			Intent intent = new Intent("com.google.zxing.client.android.SCAN");
			intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
			startActivityForResult(intent, 0);
		}

	}

	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == 0) {
			if (resultCode == RESULT_OK) {
				String contents = intent.getStringExtra("SCAN_RESULT");
				String format = intent.getStringExtra("SCAN_RESULT_FORMAT");

				CharSequence text = "Scan successful. Loading Food Product: " + contents;
				int duration = Toast.LENGTH_SHORT;

				Toast toast = Toast.makeText(getApplicationContext(), text,
						duration);
				toast.show();

				//Contents will be parsed and sent to infoscreen
				Intent intent2 = new Intent(this, InfoScreen.class);

//				Sample QR code info = "Nissin Top Ramen - Beef, 190, 760mg, 27g, 7g, 5g, 0g, false, false, false, 0g";
				
				String[] facts = contents.split(",");
				
			    String productName = "" + facts[0];
			    String calories = "Calories:" + facts[1];
			    String sodium =  "Sodium:" + facts[2];  
			    String carbs = "Carbs:" + facts[3];
			    String fat = "Fat:" + facts[4];
			    String protein = "Protein:" + facts[5];
			    String sugar = "Sugar:" + facts[6];
			    String vegetarian = "" + facts[7];
			    String vegan = "" + facts[8];
			    String gluten_free = "" + facts[9];
			    String trans_fat = 	"Trans Fat:" + facts[10];
			    
				intent2.putExtra("ProductName",productName);		
				intent2.putExtra("calories",calories);			
				intent2.putExtra("sodium",sodium);	
				intent2.putExtra("carbs",carbs);	
				intent2.putExtra("fat",fat);	
				intent2.putExtra("protein",protein);	
				intent2.putExtra("sugar",sugar);	
				intent2.putExtra("vegetarian",vegetarian);	
				intent2.putExtra("vegan",vegan);	
				intent2.putExtra("gluten_free",gluten_free);	
				intent2.putExtra("trans_fat",trans_fat);	
				
				startActivity(intent2);
				
				
				ParseQuery<ParseObject> query = ParseQuery.getQuery("Food");
				query.getInBackground(contents, new GetCallback<ParseObject>() {
					public void done(ParseObject object, ParseException e) {
						if (e == null) {
							// object will be your game score

							System.out.println("Food Found!");

							
							Food p = new Food(object
									.getString("name"), object
									.getString("id"), object
									.getString("nutrition"), object
									.getString("intro"));

							System.out.println(p.toString());
							
							
						} else {
							System.out.println("Error retrieving food info");
						}
					}
				});

				// Handle successful scan
			} else if (resultCode == RESULT_CANCELED) {
				// Handle cancel
			}
		}
	}


}
