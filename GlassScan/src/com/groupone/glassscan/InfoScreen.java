package com.groupone.glassscan;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;
import android.support.v4.app.NavUtils;
import android.annotation.TargetApi;
import android.graphics.Color;
import android.os.Build;
import com.groupone.glassscan.R;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class InfoScreen extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_info_screen);
		// Show the Up button in the action bar.
		//setupActionBar();
		 ActionBar actionBar = getActionBar();
		 actionBar.hide();
		 
		 this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);		 
		 
		 Bundle extras = getIntent().getExtras();
		 if(extras !=null) {
		     String productName = extras.getString("ProductName");
		     String calories = extras.getString("calories");
		     String sodium = extras.getString("sodium");		     
		     String carbs = extras.getString("carbs");
		     String fat = extras.getString("fat");
		     String protein = extras.getString("protein");
		     String sugar = extras.getString("sugar");
		     String vegetarian = "vegetarian";
		     String vegan = "vegan";
		     String gluten_free = "gluten_free";
		     String trans_fat = extras.getString("trans_fat");

		     TextView text1 = (TextView) findViewById(R.id.productName);
		     TextView text2 = (TextView) findViewById(R.id.calories);		     
		     TextView text3 = (TextView) findViewById(R.id.sodium);
		     TextView text4 = (TextView) findViewById(R.id.carbs);
		     TextView text5 = (TextView) findViewById(R.id.fat);
		     TextView text6 = (TextView) findViewById(R.id.protein);
		     TextView text7 = (TextView) findViewById(R.id.sugar);
		     TextView text8 = (TextView) findViewById(R.id.vegetarian);
		     TextView text9 = (TextView) findViewById(R.id.vegan);
		     TextView text10 = (TextView) findViewById(R.id.gluten_free);
		     TextView text11 = (TextView) findViewById(R.id.trans_fat2);
		     
		     text1.setText(productName);
		     text2.setText(calories);
		     text3.setText(sodium);
		     if(Integer.parseInt(sodium.substring(8,sodium.length()-2)) < 500){
		    	 text3.setTextColor(Color.parseColor("#38ce14"));		     
		     }
		     else{
		    	 text3.setTextColor(Color.parseColor("#da3e3e"));		    	 
		     }
		     text4.setText(carbs);
		     if(Integer.parseInt(carbs.substring(7,carbs.length()-1)) < 40){
		    	 text4.setTextColor(Color.parseColor("#38ce14"));		     
		     }
		     else{
		    	 text4.setTextColor(Color.parseColor("#da3e3e"));		    	 
		     }		     
		     text5.setText(fat);
		     if(Integer.parseInt(fat.substring(5,fat.length()-1)) < 20){
		    	 text5.setTextColor(Color.parseColor("#38ce14"));		     
		     }
		     else{
		    	 text5.setTextColor(Color.parseColor("#da3e3e"));		    	 
		     }		     
		     text6.setText(protein);
		     if(Integer.parseInt(protein.substring(9,protein.length()-1)) < 40){
		    	 text6.setTextColor(Color.parseColor("#38ce14"));		     
		     }
		     else{
		    	 text6.setTextColor(Color.parseColor("#da3e3e"));		    	 
		     }		     
		     text7.setText(sugar);
		     if(Integer.parseInt(sugar.substring(7,sugar.length()-1)) < 20){
		    	 text7.setTextColor(Color.parseColor("#38ce14"));		     
		     }
		     else{
		    	 text7.setTextColor(Color.parseColor("#da3e3e"));		    	 
		     }		     
		     text8.setText(vegetarian);
		     if((extras.getString("vegetarian")).equals(" true")){
		    	 text8.setTextColor(Color.parseColor("#38ce14"));
		     }
		     else{
		    	 text8.setTextColor(Color.parseColor("#da3e3e"));
		     }
		     text9.setText(vegan);
		     if((extras.getString("vegan")).equals(" true")){
		    	 text9.setTextColor(Color.parseColor("#38ce14"));
		     }
		     else{
		    	 text9.setTextColor(Color.parseColor("#da3e3e"));
		     }		     
		     text10.setText(gluten_free);
		     if((extras.getString("gluten_free")).equals(" true")){
		    	 text10.setTextColor(Color.parseColor("#38ce14"));
		     }
		     else{
		    	 text10.setTextColor(Color.parseColor("#da3e3e"));
		     }		     
		     text11.setText(trans_fat);
		     if(Integer.parseInt(trans_fat.substring(11,trans_fat.length()-1)) < 5){
		    	 text11.setTextColor(Color.parseColor("#38ce14"));		     
		     }
		     else{
		    	 text11.setTextColor(Color.parseColor("#da3e3e"));		    	 
		     }		     
		 }
		 
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.info_screen, menu);
//		return true;
//	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
