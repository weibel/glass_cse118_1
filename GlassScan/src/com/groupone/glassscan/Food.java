package com.groupone.glassscan;

public class Food {
	private String name;
	private String id;  //id for picture chosen
	private String nutrition;
	private String intro;
	
	public Food(){
		
	}
	public Food(String r_name, String r_id, String r_nutrition, String r_intro){
		this.name = r_name;
		this.id = r_id;
		this.nutrition = r_nutrition;
		this.intro = r_intro;
	}
	
	public String getName(){
		return name;
	}
	
    public String getId(){
    	return id;
    }
    public String getNutrition(){
    	return nutrition;
    }
    public String getIntro(){
    	return intro;
    }
    public String toString(){
        return new String("name:" + name + "\nid:" + id + "\nnutrition:" + nutrition + "\ndescription"
        		          + intro + "\n");	
    }
     
}