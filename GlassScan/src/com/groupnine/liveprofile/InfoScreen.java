package com.groupnine.liveprofile;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.support.v4.app.NavUtils;
import android.annotation.TargetApi;
import android.graphics.Color;
import android.os.Build;
import com.groupone.glassscan.R;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class InfoScreen extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_info_screen);
		// Show the Up button in the action bar.
		//setupActionBar();
		 ActionBar actionBar = getActionBar();
		 actionBar.hide();
		 
		 Bundle extras = getIntent().getExtras();
		 if(extras !=null) {
		     String productName = extras.getString("ProductName");
		     String calories = extras.getString("calories");
		     String sodium = extras.getString("sodium");		     
		     String carbs = extras.getString("carbs");
		     String fat = extras.getString("fat");
		     String protein = extras.getString("protein");
		     String sugar = extras.getString("sugar");
		     String vegetarian = extras.getString("vegetarian");
		     String vegan = extras.getString("vegan");
		     String gluten_free = extras.getString("gluten_free");
		     String trans_fat = extras.getString("trans_fat");

		     TextView text1 = (TextView) findViewById(R.id.productName);
		     TextView text2 = (TextView) findViewById(R.id.calories);		     
		     TextView text3 = (TextView) findViewById(R.id.sodium);
		     TextView text4 = (TextView) findViewById(R.id.carbs);
		     TextView text5 = (TextView) findViewById(R.id.fat);
		     TextView text6 = (TextView) findViewById(R.id.protein);
		     TextView text7 = (TextView) findViewById(R.id.sugar);
		     TextView text8 = (TextView) findViewById(R.id.vegetarian);
		     TextView text9 = (TextView) findViewById(R.id.vegan);
		     TextView text10 = (TextView) findViewById(R.id.gluten_free);
		     TextView text11 = (TextView) findViewById(R.id.trans_fat);
		     
		     text1.setText(productName);
		     text2.setText(calories);
		     text3.setText(sodium);
		     text4.setText(carbs);
		     text5.setText(fat);
		     text6.setText(protein);
		     text7.setText(sugar);
		     text8.setText(vegetarian);
		     text9.setText(vegan);
		     text10.setText(gluten_free);
		     text11.setText(trans_fat);		     
		     
//		     text2.setText("Sodium is " + sodium);
//		     if(sodium.equals("> 20%")){
//		    	 text2.setTextColor(Color.parseColor("#f02828"));
//		     }
		     
		 }
		 
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.info_screen, menu);
//		return true;
//	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
