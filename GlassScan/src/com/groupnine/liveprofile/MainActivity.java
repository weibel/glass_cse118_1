package com.groupnine.liveprofile;

import android.os.Build;
import android.os.Bundle;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.view.Menu;
import android.view.WindowManager;
import android.content.Intent;
import android.view.View;
//import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.groupnine.liveprofile.Food;
import com.groupone.glassscan.R;
import com.parse.*;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class MainActivity extends Activity implements OnClickListener {

	private Button scanBtn;
	//private TextView firstNameTxt, lastNameTxt, ageTxt, genderTxt, IDTxt;
	private TextView nameTxt, idTxt, nutritionTxt, introTxt;	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// requestWindowFeature(Window.FEATURE_NO_TITLE);

		System.out.println("About to initialise Parse");
		Parse.initialize(this, "CNWcbJXoR5UDVjlsyiMvL3IrxJefNSbyhmbSpuWt",
				"mdEtEBxNcyxeKzxLjxh1ydaZBhBZ1sbdaztRT11c");
		System.out.println("Parse: tracking app opening");
		ParseAnalytics.trackAppOpened(getIntent());

		setScanBtn((Button) findViewById(R.id.scan_button));
		// setFormatTxt((TextView) findViewById(R.id.scan_format));
		// setContentTxt((TextView) findViewById(R.id.scan_content));

//		 setIDTxt((TextView) findViewById(R.id.ID));
//		 setFirstNameTxt((TextView) findViewById(R.id.first_name));
//		 setLastNameTxt((TextView) findViewById(R.id.last_name));
//		 setAgeTxt((TextView) findViewById(R.id.age));
//		 setGenderTxt((TextView) findViewById(R.id.gender));
//		 
//		 
//		 ageTxt.setText("Age: ");
//		 genderTxt.setText("Gender: ");
//		 firstNameTxt.setText("First Name: ");
//		 lastNameTxt.setText("Last Name: ");
//		 IDTxt.setText("ID: ");
		
		 ActionBar actionBar = getActionBar();
		 actionBar.hide();
		 
		//Remove title bar
		 //this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		 //Remove notification bar
		 this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		 
		scanBtn.setOnClickListener(this);

	}
	

//	public static TimelineItem insertTimelineItem(Mirror service, String text,
//			String contentType, InputStream attachment, String notificationLevel) {
//		TimelineItem timelineItem = new TimelineItem();
//		timelineItem.setText(text);
//		if (notificationLevel != null && notificationLevel.length() > 0) {
//			timelineItem.setNotification(new NotificationConfig()
//					.setLevel(notificationLevel));
//		}
//		try {
//			if (contentType != null && contentType.length() > 0
//					&& attachment != null) {
//				// Insert both metadata and attachment.
//
//				InputStreamContent mediaContent = new InputStreamContent(
//						contentType, attachment);
//				return service.timeline().insert(timelineItem, mediaContent)
//						.execute();
//
//			} else {
//				// Insert metadata only.
//				return service.timeline().insert(timelineItem).execute();
//			}
//		} catch (IOException e) {
//			System.err.println("An error occurred: " + e);
//			return null;
//		}
//	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void showInfoScreen(View view){
		Intent intent = new Intent(this, InfoScreen.class);
	    String productName = "Nissan Top Ramen - Beef";
	    String calories = "Calories: 190";
	    String sodium =  "Sodium: 760mg - 32%";  
	    String carbs = "Carbs: 27g - 9%";
	    String fat = "Fat: 7g - 11%";
	    String protein = "Protein: 5g";
	    String sugar = "Sugars: 0g";
	    String vegetarian = "Vegetarian";
	    String vegan = "Vegan";
	    String gluten_free = "Gluten Free";
	    String trans_fat = 	"Trans_Fat";
	    
		intent.putExtra("ProductName",productName);		
		intent.putExtra("calories",calories);			
		intent.putExtra("sodium",sodium);	
		intent.putExtra("carbs",carbs);	
		intent.putExtra("fat",fat);	
		intent.putExtra("protein",protein);	
		intent.putExtra("sugar",sugar);	
		intent.putExtra("vegetarian",vegetarian);	
		intent.putExtra("vegan",vegan);	
		intent.putExtra("gluten_free",gluten_free);	
		intent.putExtra("trans_fat",trans_fat);	
		
		startActivity(intent);
	}

	public Button getScanBtn() {
		return scanBtn;
	}

	public void setScanBtn(Button scanBtn) {
		this.scanBtn = scanBtn;
	}

	// public TextView getFormatTxt() {
	// return formatTxt;
	// }
	//
	// public void setFormatTxt(TextView formatTxt) {
	// this.formatTxt = formatTxt;
	// }
	//
	// public TextView getContentTxt() {
	// return contentTxt;
	// }
	//
	// public void setContentTxt(TextView contentTxt) {
	// this.contentTxt = contentTxt;
	// }
//
//	public TextView getAgeTxt() {
//		return ageTxt;
//	}
//
//	public void setAgeTxt(TextView ageTxt) {
//		this.ageTxt = ageTxt;
//	}
//
//	public TextView getGenderTxt() {
//		return genderTxt;
//	}
//
//	public void setGenderTxt(TextView genderTxt) {
//		this.genderTxt = genderTxt;
//	}
//
//	public TextView getFirstNameTxt() {
//		return firstNameTxt;
//	}
//
//	public void setFirstNameTxt(TextView firstNameTxt) {
//		this.firstNameTxt = firstNameTxt;
//	}
//
//	public TextView getLastNameTxt() {
//		return lastNameTxt;
//	}
//
//	public void setLastNameTxt(TextView lastNameTxt) {
//		this.lastNameTxt = lastNameTxt;
//	}
//
//	public TextView getIDTxt() {
//		return IDTxt;
//	}
//
//	public void setIDTxt(TextView IDTxt) {
//		this.IDTxt = IDTxt;
//	}

	@Override
	public void onClick(View v) {
		// respond to clicks for OnClickListeners
		if (v.getId() == R.id.scan_button) {
			// scan
			// IntentIntegrator scanIntegrator = new IntentIntegrator(this);
			// scanIntegrator.initiateScan();

			Intent intent = new Intent("com.google.zxing.client.android.SCAN");
			intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
			startActivityForResult(intent, 0);
		}

	}

	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == 0) {
			if (resultCode == RESULT_OK) {
				String contents = intent.getStringExtra("SCAN_RESULT");
				String format = intent.getStringExtra("SCAN_RESULT_FORMAT");

				// formatTxt.setText("FORMAT: " + format);
				// contentTxt.setText("CONTENT: " + contents);

				CharSequence text = "Scan successful. Loading Food Product: " + contents;
				int duration = Toast.LENGTH_SHORT;

				Toast toast = Toast.makeText(getApplicationContext(), text,
						duration);
				toast.show();

				//Need to parse contents and then send to infoscreen
				Intent intent2 = new Intent(this, InfoScreen.class);
				String productName = "Apple";
				String Sodium = "> 20%";
				intent2.putExtra("ProductName",contents);		
				intent2.putExtra("Sodium",Sodium);				
				startActivity(intent2);
				
				ParseQuery<ParseObject> query = ParseQuery.getQuery("Food");
				query.getInBackground(contents, new GetCallback<ParseObject>() {
					public void done(ParseObject object, ParseException e) {
						if (e == null) {
							// object will be your game score

							System.out.println("Food Found!");

							
							Food p = new Food(object
									.getString("name"), object
									.getString("id"), object
									.getString("nutrition"), object
									.getString("intro"));

							System.out.println(p.toString());
							
							
//							 ageTxt.setText("Age: " + p.getAge());
//							 genderTxt.setText("Gender: " + p.getGender());
//							 firstNameTxt.setText("First Name: " + p.getFirstName());
//							 lastNameTxt.setText("Last Name: " + p.getLastName());
//							 IDTxt.setText("ID: " + p.getPatientID());
							
							

						} else {
							System.out.println("Error retrieving food info");
						}
					}
				});

				// Handle successful scan
			} else if (resultCode == RESULT_CANCELED) {
				// Handle cancel
			}
		}
	}

	// public void onActivityResult(int requestCode, int resultCode, Intent
	// intent) {
	// //retrieve scan result
	// IntentResult scanningResult =
	// IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
	// if (scanningResult != null) {
	//
	// String scanContent = scanningResult.getContents();
	// String scanFormat = scanningResult.getFormatName();
	//
	// formatTxt.setText("FORMAT: " + scanFormat);
	// contentTxt.setText("CONTENT: " + scanContent);
	//
	// }else{
	// Toast toast = Toast.makeText(getApplicationContext(),
	// "No scan data received!", Toast.LENGTH_SHORT);
	// toast.show();
	// }
	//
	//
	//
	// }

}
